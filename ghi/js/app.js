function createCard(name, description, pictureUrl,starts, ends, subName) {
  return `

  <div class="card shadow p-0 mb-4 bg-white rounded">
      <img src="${pictureUrl}" class="card-img-top">
      <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${subName}</h6>
          <p class="card-text">${description}</p>
      </div>
  </div>
  <div class="card-footer text-muted">
          ${starts} - ${ends}
          </div>

  `;
}

function createError(text){
  return `
  <div class="alert alert-danger" role="alert">
      ${text}
      </div>
  `;
}


window.addEventListener('DOMContentLoaded', async () => {

  const url = 'http://localhost:8000/api/conferences/';
  const columns = document.querySelectorAll('.col')
  let colIndx = 0;

  try {
      const response = await fetch(url);
      console.log(response);
      if (!response.ok) {
      // Figure out what to do when the response is bad
          const html = createError("The URL can not be accessed");
          const column = columns[1];
          column.innerHTML += html;

      } else {
      const data = await response.json();

      for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
              const details = await detailResponse.json();
              const name = details.conference.name;
              const description = details.conference.description;
              const pictureUrl = details.conference.location.picture_url;
              const starts = new Date(details.conference.starts);
              const ends = new Date(details.conference.ends);
              const subName = details.conference.location.name;
              const html = createCard(name, description, pictureUrl, starts.toLocaleDateString('en-US'), ends.toLocaleDateString('en-US'), subName);

              const column = columns[colIndx % 3];
              column.innerHTML += html;
              colIndx = (colIndx + 1) % 3;
              console.log(html);
          }
      }

      }
  } catch (e) {
    // Figure out what to do if an error is raised
      `<div class="alert alert-danger" role="alert">
      A simple danger alert—check it out!
      Error code ${e}
      </div>`;
  }

});
